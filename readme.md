# deep-condition
 
Simple and lightweight library for filtering arrays with predefined human-friendly conditions
For usage in browser use file `source/index.js`

## Install

Node

```
npm install deep-condition --save
```
 
## Usage

```javascript
let filterFunction = require('deep-condition');

let sampleData = [
    {"rank": 1, "rating": 9.2, "year": null, "title": "The Shawshank Redemption"},
    {"rank": 2, "rating": 9.2, "year": 1972, "title": "The Godfather"},
    {"rank": 3, "rating": 9, "year": 1974, "title": "The Godfather: Part II"},
    {"rank": 4, "rating": 8.9, "year": 1966, "title": "Il buono, il brutto, il cattivo."},
    {"rank": 5, "rating": 8.9, "year": 1994, "title": "Pulp Fiction"},
    {"rank": 6, "rating": 8.9, "year": 1957, "title": "12 Angry Men"},
    {"rank": 7, "rating": 8.9, "year": 1993, "title": "Schindler's List"},
    {"rank": 8, "rating": 8.8, "year": 1975, "title": "One Flew Over the Cuckoo's Nest"},
    {"rank": 9, "rating": 8.8, "year": 2010, "title": "Inception"},
    {"rank": 0, "rating": 8.8, "year": 2008, "title": "The Dark Knight"}
];

let filter = {
    logic: 'or',
    filters: [
        {
            logic: 'and',
            filters: [
                {field: 'rating', operator: 'eq', value: 8.8},
                {field: 'year', operator: 'gt', value: 2000}
            ]
        },
        {field: 'title', operator: 'contains', value: 'Godfather'}
    ]
};

let result = filterFunction(sampleData, filter);

console.dir(result);

/** OUTPUT:
[ { rank: 2, rating: 9.2, year: 1972, title: 'The Godfather' },
  { rank: 3, rating: 9, year: 1974, title: 'The Godfather: Part II' },
  { rank: 9, rating: 8.8, year: 2010, title: 'Inception' },
  { rank: 0, rating: 8.8, year: 2008, title: 'The Dark Knight' } ]
**/
```

## Description

### Types of filter conditions (EXAMPLES)

#### Filter group
```javascript
let filterExample = {
    logic: 'or',
    filters: [
        {
            logic: 'and',
            filters: [
                {field: 'rating', operator: 'eq', value: 8.8},
                {field: 'year', operator: 'gt', value: 2000}
            ]
        },
        {field: 'title', operator: 'contains', value: 'Godfather'}
    ]
};
```

#### Simple condition
```javascript
let filterExample = {field: 'rating', operator: 'eq', value: 8.8};
```

#### Array of simple conditions
By default uses logic: __AND__
```javascript
let filterExample = [{field: 'rating', operator: 'eq', value: 8.8}, {field: 'year', operator: 'eq', value: 2010}];
```

```javascript
let resultFiltering = filterFunction(sampleData, filterExample);
```

### Supported types of conditions

* eq - __equal__
```javascript
let filter = {field: 'rating', operator: 'eq', value: 8.8};
```
* neq - __not equal__
```javascript
let filter = {field: 'rating', operator: 'neq', value: 8.8};
```
* isnull - __is null__
```javascript
let filter = {field: 'year', operator: 'isnull'};
```
* isnotnull - __is not null__
```javascript
let filter = {field: 'year', operator: 'isnotnull'};
```
* lt - __less than__
```javascript
let filter = {field: 'rating', operator: 'lt', value: 8.9};
```
* lte - __less than or equal__
```javascript
let filter = {field: 'rating', operator: 'lte', value: 8.9};
```
* gt - __greater than__
```javascript
let filter = {field: 'rating', operator: 'gt', value: 8.9};
```
* gte - __greater than or equal__
```javascript
let filter = {field: 'rating', operator: 'gte', value: 8.9};
```
* startswith - __starts with__
```javascript
let filter = {field: 'title', operator: 'startswith', value: 'The'};
```
* endswith - __ends with__
```javascript
let filter = {field: 'title', operator: 'endswith', value: 'tion'};
```
* contains - __contains__
```javascript
let filter = {field: 'title', operator: 'contains', value: 'Godfather'};
```
* doesnotcontain - __does not contain__
```javascript
let filter = {field: 'title', operator: 'doesnotcontain', value: 'Godfather'};
```
* isempty - __is empty__
```javascript
let filter = {field: 'rank', operator: 'isempty'};
```
* isnotempty - __is not empty__
```javascript
let filter = {field: 'rank', operator: 'isnotempty'};
```

__FOR MORE EXAMPLES SEE `test/filtering.js` file in package__
__FOR JSDOC DESCRIPTIONS SEE `source/index.js` file in package__

## Test
 
Test

```
npm install
npm run test
```

## Change list

### Version 1.0.0

* Initial commit