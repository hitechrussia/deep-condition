/**
 * @typedef {'eq'|'neq'|'isnull'|'isnotnull'|'lt'|'lte'|'gt'|'gte'|'startswith'|'endswith'|'contains'|'doesnotcontain'|'isempty'|'isnotempty'} ConditionType
 */

/**
 * @typedef {'or'|'and'} LogicOperator
 */

/**
 * @typedef {{field: string, operator: ConditionType, value: any}} FilterCondition
 */

/**
 * @typedef {{}} FilterConditionGroup
 * @property {LogicOperator} logic
 * @property {FilterCondition[]} filters
 */

/**
 * @typedef {FilterCondition|FilterConditionGroup|FilterCondition[]} Filter
 */

/**
 * @param {{}} row
 * @param {FilterCondition} filterRow
 */
function filterDataRowWithSimpeFilter(row, filterRow) {
    "use strict";
    let rowValue = row[filterRow.field];
    let conditionValue = filterRow.value;

    switch (filterRow.operator) {
        case 'eq':
            return rowValue == conditionValue;
            break;
        case 'neq':
            return rowValue != conditionValue;
            break;
        case 'isnull':
            return rowValue === null;
            break;
        case 'isnotnull':
            return rowValue !== null;
            break;
        case 'lt':
            return +(rowValue) < +(conditionValue);
            break;
        case 'lte':
            return +(rowValue) <= +(conditionValue);
            break;
        case 'gt':
            return +(rowValue) > +(conditionValue);
            break;
        case 'gte':
            return +(rowValue) >= +(conditionValue);
            break;
        case 'startswith':
            return rowValue.toString().indexOf(conditionValue.toString()) === 0;
            break;
        case 'endswith':
            let conditionIndex = rowValue.toString().indexOf(conditionValue.toString());
            return conditionIndex + conditionValue.toString().length === rowValue.toString().length;
            break;
        case 'contains':
            return rowValue.toString().indexOf(conditionValue.toString()) !== -1;
            break;
        case 'doesnotcontain':
            return rowValue.toString().indexOf(conditionValue.toString()) === -1;
            break;
        case 'isempty':
            if (typeof rowValue === 'string') {
                return rowValue === '';
            } else if (Array.isArray(rowValue)) {
                return rowValue.length === 0;
            } else if (typeof rowValue === 'object') {
                return Object.keys(rowValue).length === 0;
            } else if (typeof rowValue === 'number') {
                return rowValue === 0;
            } else {
                return !rowValue
            }
            break;
        case 'isnotempty':
            if (typeof rowValue === 'string') {
                return rowValue !== '';
            } else if (Array.isArray(rowValue)) {
                return rowValue.length !== 0;
            } else if (typeof rowValue === 'object') {
                return Object.keys(rowValue).length !== 0;
            } else if (typeof rowValue === 'number') {
                return rowValue !== 0;
            } else {
                return !!rowValue;
            }
            break;
        default:
            throw new Error(`Unknow condition operator: ${filterRow.operator}`);
            break;
    }
}

/**
 * @param {{}} row
 * @param {FilterConditionGroup} filterGroup
 */
function filterDataRowWithGroupFilters(row, filterGroup) {
    "use strict";
    let length = filterGroup.filters.length;

    if (filterGroup.logic === 'and') {
        for(let i = 0; i < length; i++) {
            if (!filterDataRow(row, filterGroup.filters[i])) {
                return false;
            }
        }

        return true;
    } else {
        for(let i = 0; i < length; i++) {
            if (filterDataRow(row, filterGroup.filters[i])) {
                return true;
            }
        }

        return false;
    }

}

/**
 * Filters one row by filters
 * @param {{}} row
 * @param {Filter} filters
 *
 * @return {boolean}
 */
function filterDataRow(row, filters) {
    "use strict";

    if (Array.isArray(filters)) {
        return filterDataRowWithGroupFilters(row, {
            logic: 'and',
            filters: filters
        });
    } else if (filters.hasOwnProperty('logic')) {
        return filterDataRowWithGroupFilters(row, filters);
    } else {
        return filterDataRowWithSimpeFilter(row, filters);
    }
}

/**
 * Filters data result
 * @param {{}[]} data
 * @param {Filter} filters
 *
 * @returns {{}[]}
 */
function filterData(data, filters) {
    "use strict";

    return data.filter((x) => {
        return filterDataRow(x, filters);
    });

}

module.exports = filterData;