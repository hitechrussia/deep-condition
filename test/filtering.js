let {expect} = require('chai');

let filterFunction = require('../index');

let sampleData = [
    {"rank": 1, "rating": 9.2, "year": null, "title": "The Shawshank Redemption"},
    {"rank": 2, "rating": 9.2, "year": 1972, "title": "The Godfather"},
    {"rank": 3, "rating": 9, "year": 1974, "title": "The Godfather: Part II"},
    {"rank": 4, "rating": 8.9, "year": 1966, "title": "Il buono, il brutto, il cattivo."},
    {"rank": 5, "rating": 8.9, "year": 1994, "title": "Pulp Fiction"},
    {"rank": 6, "rating": 8.9, "year": 1957, "title": "12 Angry Men"},
    {"rank": 7, "rating": 8.9, "year": 1993, "title": "Schindler's List"},
    {"rank": 8, "rating": 8.8, "year": 1975, "title": "One Flew Over the Cuckoo's Nest"},
    {"rank": 9, "rating": 8.8, "year": 2010, "title": "Inception"},
    {"rank": 0, "rating": 8.8, "year": 2008, "title": "The Dark Knight"}
];

describe('Filtering', () => {
    "use strict";

    it('Simple condition', () => {

        let result = filterFunction(sampleData, {field: 'rating', operator: 'eq', value: 8.8});
        expect(result.length).equal(3);

    });

    it('Group condition', () => {

        let filter = {
            logic: 'and',
            filters: [
                {field: 'rating', operator: 'eq', value: 8.8},
                {field: 'year', operator: 'gt', value: 2000}
            ]
        };

        let result = filterFunction(sampleData, filter);
        expect(result.length).equal(2);

    });

    it('Deep group condition', () => {

        let filter = {
            logic: 'or',
            filters: [
                {
                    logic: 'and',
                    filters: [
                        {field: 'rating', operator: 'eq', value: 8.8},
                        {field: 'year', operator: 'gt', value: 2000}
                    ]
                },
                {field: 'title', operator: 'contains', value: 'Godfather'}
            ]
        };

        let result = filterFunction(sampleData, filter);

        expect(result.length).equal(4);

    });

    it('Unknown condition operator throws error', (done) => {
        let filter = {field: 'rating', operator: 'foo', value: 8.8};

        let p = new Promise((res, rej) => {
            return filterFunction(sampleData, filter);
        });

        p.then((data) => {
            expect(data).to.be.an.instanceOf(Error);
            done();
        }).catch((err) => {
            expect(err).to.be.an.instanceOf(Error);
            done();
        });
    });

    describe('Filtering operators', () => {

        it('eq', () => {
            let filter = {field: 'rating', operator: 'eq', value: 8.8};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(3);
        });

        it('neq', () => {
            let filter = {field: 'rating', operator: 'neq', value: 8.8};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(7);
        });

        it('isnull', () => {
            let filter = {field: 'year', operator: 'isnull'};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(1);
        });

        it('isnotnull', () => {
            let filter = {field: 'year', operator: 'isnotnull'};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(9);
        });

        it('lt', () => {
            let filter = {field: 'rating', operator: 'lt', value: 8.9};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(3);
        });

        it('lte', () => {
            let filter = {field: 'rating', operator: 'lte', value: 8.9};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(7);
        });

        it('gt', () => {
            let filter = {field: 'rating', operator: 'gt', value: 8.9};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(3);
        });

        it('gte', () => {
            let filter = {field: 'rating', operator: 'gte', value: 8.9};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(7);
        });

        it('startswith', () => {
            let filter = {field: 'title', operator: 'startswith', value: 'The'};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(4);
        });

        it('endswith', () => {
            let filter = {field: 'title', operator: 'endswith', value: 'tion'};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(3);
        });

        it('contains', () => {
            let filter = {field: 'title', operator: 'contains', value: 'Godfather'};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(2);
        });

        it('doesnotcontain', () => {
            let filter = {field: 'title', operator: 'doesnotcontain', value: 'Godfather'};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(8);
        });

        it('isempty', () => {
            let filter = {field: 'rank', operator: 'isempty'};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(1);
        });

        it('isnotempty', () => {
            let filter = {field: 'rank', operator: 'isnotempty'};
            let result = filterFunction(sampleData, filter);

            expect(result.length).equal(9);
        });

    });

});